<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;

class FacebookController extends Controller
{
    //
    public function redirectTofacebook(){
        return Socialite::driver('facebook')->redirect();
    }
    
    public function handlefacebookCallback() {
        $facebookUser = Socialite::driver('facebook')->stateless()->user();
        return back()->with('message','Se ha producido un error')->with('typealert','danger'); 
        // try {
        //     $facebookUser = Socialite::driver('facebook')->stateless()->user();
        //     $findUser = User::where('facebook_id', $facebookUser->id)->orWhere('email', $facebookUser->email)->first();

        //     if ($findUser) {
        //         Auth::login($findUser, true); // Activar "recordarme"
        //     } else {
        //         $user = User::create([
        //             'name' => $facebookUser->name,
        //             'lastname' => $facebookUser->user['family_name'] ?? 'N/A',
        //             'email' => $facebookUser->email,
        //             'facebook_id' => $facebookUser->id,
        //             'password' => encrypt('default_password'), // Puedes cambiar esto
        //         ]);
        //         Auth::login($user, true); // Activar "recordarme"
        //     }

        //     return redirect('/'); // Cambia esto a la ruta que quieras redirigir después del login
            
        // } catch (Exception $e) {
        //     return redirect('/login')->with('error', 'No se pudo autenticar con Google, intente de nuevo.');
        // }
    }
}
