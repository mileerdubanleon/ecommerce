<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// model
use App\Models\Carrusel;
use Auth, Validator;

class CarruselController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
    	// $this->middleware('user.status');
        // $this->middleware('user.permissions');
        $this->middleware('isadmin');
    }

    // view carrusel table
    public function getHome(){
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'carrusel_list')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver la lista de carrusel.');
        }
    	// Obtener todos los elementos del carrusel
        $carr = Carrusel::orderBy('id', 'Asc')->get();

        // Pasar los datos a la vista
        return view('admin.carrusel.home', ['carr' => $carr]);
    }

    public function postCarruselAdd(Request $request){
        $rules = [
    		'title' => 'required',
            'textla' => 'required',
            'linkla' => 'required',
    	];
    	$messages = [
    		'name.required' => 'Se requiere de un titulo para el carrusel.',
            'textla.required' => 'Se requiere de un texto para el carrusel.',
            'linkla.required' => 'Se requiere de un link para el carrusel.'
    	];
    	$validator = Validator::make($request->all(), $rules, $messages);

    	if($validator->fails()):
    		return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger'); 
    	else:

    		$c = new Carrusel;
    		$c->text1 = $request->input('title');
			$c->text2 = $request->input('description');
    		$c->link_text = $request->input('textla');
    		$c->link_url = $request->input('linkla');
    		if($c->save()):
    			return back()->with('message', 'Guardado con éxito')->with('typealert', 'success');
    		endif;
    	endif;
    }
}
