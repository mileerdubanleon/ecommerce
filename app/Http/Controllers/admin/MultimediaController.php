<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Multimedia;
use Illuminate\Support\Facades\DB;
use Auth;

class MultimediaController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
    	// $this->middleware('user.status');
        // $this->middleware('user.permissions');
        $this->middleware('isadmin');
    }

    // view 
    public function getHome(){
        // Verificar si el usuario tiene permisos para editar
        if (!kvfj(Auth::user()->permissions, 'video_list')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver la lista de multimedia.');
        }
        $multimedia = Multimedia::first(); // Obtener el primer (y único) registro de la tabla multimedia
        return view('admin.multimedia.home', compact('multimedia'));
    }

    // upload image
    public function postUpload(Request $request){
        $request->validate([
            'video_url' => 'required|file|mimes:mp4,mov,avi,flv|max:10240',
            'title_video' => 'required|string|max:255',
            'description_video' => 'required|string|max:255',
            'link_video' => 'nullable|url'
        ]);

        // Eliminar el video antiguo si existe
        $multimedia = Multimedia::first();
        if ($multimedia) {
            Storage::disk('uploads_video_home')->delete($multimedia->video_url);
            $multimedia->delete();
        }

         // Subir el nuevo video y guardarlo como "home.mp4"
         $videoPath = $request->file('video_url')->storeAs('', 'home.mp4', 'uploads_video_home');

        // Guardar la información en la BD
        Multimedia::create([
            'title' => $request->input('title_video'),
            'description' => $request->input('description_video'),
            'video_url' => $videoPath,
            'link_url' => $request->input('link_video')
        ]);

        return redirect()->route('multimedia')->with('message', 'Video subido y guardado correctamente.')->with('typealert', 'success');
    }
}
