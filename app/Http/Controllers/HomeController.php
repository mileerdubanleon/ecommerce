<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Multimedia;
use App\Models\Carrusel;

class HomeController extends Controller
{
    //view index
    public function index(){
        // carrusel view
        $carouselItems = Carrusel::all();
        // Obtener el primer (y único) registro de la tabla multimedia
        $multimedia = Multimedia::first();
        return view('public.home', compact('carouselItems', 'multimedia'));
    }

}
