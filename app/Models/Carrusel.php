<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carrusel extends Model
{
    use HasFactory;

    protected $dates = ['delete_at'];
    protected $table = 'carousel_items';
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['text1', 'text2', 'link_text', 'link_url'];
}
