<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    use HasFactory;

    protected $dates = ['delete_at'];
    protected $table = 'multimedia';
    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'title',
        'description',
        'video_url',
        'link_url',
    ];
}
