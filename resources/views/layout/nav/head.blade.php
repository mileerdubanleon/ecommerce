<div class="d-flex container-fluid container-movile" style="background-color: #f5f5f5;">

    @if (Auth::check())
        <div class="d-flex container-fluid" style="background-color: #f5f5f5;">
            <div class="container-header">
                <a class="link-item " href="#">Quiénes somos</a>
                <span>|</span>

                <a class="link-item " href="#">Contacto</a>
                <span>|</span>

                <a class="link-item " href="#">Ayuda</a>
                <span>|</span>

                <a class="link-item" href="{{ url('/') }}">
                    Inicio
                </a><span>|</span>

                <a class="dropdown ">
                    <a class="dropdown-toggle p-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }} 
                        <span>

                        </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <!-- Agrega aquí las opciones del dropdown -->
                        <a class="dropdown-item" href="{{ url('/account/edit') }}">Cuenta</a>
                        <a class="dropdown-item" href="#">Configuración</a>
                        @if (Auth::user()->role == 1)
                            <a class="dropdown-item" href="{{ url('/admin') }}">Administración</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('/logout') }}">Cerrar sesión</a>
                    </div>
                </a>
                
            </div>
        </div>
    @else:

    <div class="container-header">
        <a class="link-item " href="#">Quiénes somos</a>
        <span>|</span>

        <a class="link-item " href="#">Contacto</a>
        <span>|</span>

        <a class="link-item " href="#">Ayuda</a>
        <span>|</span>
        <a class="link-item" href="{{ url('/register') }}">Únete a nosotros</a><span>|</span>
        <a class="link-item" href="{{ url('login') }}">Iniciar sesión</a>
    </div>
    @endif
</div>

