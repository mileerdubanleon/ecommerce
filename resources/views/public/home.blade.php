@extends('index')

@section('content')

    {{-- Carousel Texto --}}
    @include('component.carousel')
    
    {{-- Video home --}}
    @include('component.video')

    {{-- section carrusel infinite --}}
    @include('component.cards_carrusel_product')

    {{-- section carrusel card product --}}
    @include('component.card_products')

@endsection