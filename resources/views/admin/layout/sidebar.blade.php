<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar" style="background-color: #f5f5f5;">
    <button id="sidebar-toggle-btn" class="btn btn-light d-lg-none">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-menu-button-wide" viewBox="0 0 16 16">
            <path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v2A1.5 1.5 0 0 1 14.5 5h-13A1.5 1.5 0 0 1 0 3.5zM1.5 1a.5.5 0 0 0-.5.5v2a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-2a.5.5 0 0 0-.5-.5z"/>
            <path d="M2 2.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5m10.823.323-.396-.396A.25.25 0 0 1 12.604 2h.792a.25.25 0 0 1 .177.427l-.396.396a.25.25 0 0 1-.354 0M0 8a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm1 3v2a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2zm14-1V8a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2zM2 8.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5m0 4a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5"/>
        </svg>
        
    </button>
    <div class="p-4 card">
        <span class="font-weight-bold">{{ Auth::user()->name }} </span>
        <span style="font-size: 10px;">{{ Auth::user()->email }} </span>
        <span style="font-size: 10px;">{{ Auth::user()->phone }} </span>
        <a href="#" style="font-size: 12px;">Ver perfil</a>
    </div>

    @include('admin.layout.session')

    <ul class="sidebar-nav px-4 my-2 py-1" id="sidebar-nav">
        <span class="border-bottom bg-dark text-white border-r-3 shadow px-2">Administración</span>
        @if(kvfj(Auth::user()->permissions, 'dashboard'))
            <li class="nav-item">
                <a href="{{ url('admin') }}" class="nav-link lk-dashboard {{ Route::currentRouteName() == 'lk-dashboard' ? 'active' : '' }}">
                    <span>Dashboard</span>
                </a>
            </li><!-- End Dashboard Nav -->
        @endif

        @if(kvfj(Auth::user()->permissions, 'user_list'))
            <li class="nav-item">
                <a class="nav-link lk-user_list lk-user_edit_get lk-user_permissions_get" href="{{ url('/admin/users/all') }}">
                    <span>Usuarios</span>
                </a>
            </li><!-- End Users Nav -->
        @endif
        <span class="border-bottom bg-dark text-white border-r-3 shadow px-2">Tienda</span>

        @if(kvfj(Auth::user()->permissions, 'categories'))
            
            <li class="nav-item">
                <a class="nav-link lk-categories lk-category_add lk-category_edit_get lk-category_subs lk-category_delete" href="{{ url('/admin/categories/0') }}">
                    <span>Categorias</span>
                </a>
            </li><!-- End categories Nav -->
        @endif

        @if(kvfj(Auth::user()->permissions, 'products'))
            <li class="nav-item">
                <a class="nav-link lk-products lk-product_add lk-product_search lk-product_edit lk-product_inventory lk-edit_inventory lk-edit_inventory lk-product_gallery_add" href="{{ url('/admin/products/all') }}">
                    <span>Productos</span>
                </a>
            </li><!-- End product Nav -->
        @endif

        @if(kvfj(Auth::user()->permissions, 'orders_list'))
            <li class="nav-item">
                <a class="nav-link lk-orders_list" href="{{ url('/admin/order') }}">
                    <span>Ordenes</span>
                </a>
            </li><!-- End order Nav -->
        @endif

        <span class="border-bottom bg-dark text-white border-r-3 shadow px-2">Configuracion multimedia</span>
        @if(kvfj(Auth::user()->permissions, 'carrusel_list'))
            <li class="nav-item">
                <a class="nav-link lk-carrusel lk-carrusel_edit " href="{{ url('/admin/carrusel') }}">
                    <span>Carrusel texto</span>
                </a>
            </li><!-- End carrusel Nav -->
            
        @endif

        @if(kvfj(Auth::user()->permissions, 'video_list'))
            <li class="nav-item">
                <a class="nav-link lk-multimedia" href="{{ url('/admin/multimedia') }}">
                    <span>Video de inicio</span>
                </a>
            </li><!-- End video Nav -->
        @endif

        @if(kvfj(Auth::user()->permissions, 'settings'))
            <li class="nav-item">
                <a class="nav-link lk-settings inline-block justify-content-center " href="{{ url('/admin/settings') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-gear" viewBox="0 0 18 18">
                        <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492M5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0"/>
                        <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115z"/>
                      </svg>
                    <span class="text-center">Configuración</span>
                </a>
            </li><!-- End order Nav -->
        @endif

    </ul>
    
</aside><!-- End Sidebar-->


<script>
    // routes active
    document.addEventListener('DOMContentLoaded', function() {
        const sidebar = document.getElementById('sidebar');
        const sidebarToggleButton = document.getElementById('sidebar-toggle-btn');

        sidebarToggleButton.addEventListener('click', function() {
            sidebar.classList.toggle('collapsed');
        });

    let route = document.querySelector('meta[name="routeName"]').getAttribute('content');
    let menuLink = document.querySelector('.lk-' + route);
    
    if (menuLink) {
        menuLink.classList.add('active-route');
    }

    });
  </script>