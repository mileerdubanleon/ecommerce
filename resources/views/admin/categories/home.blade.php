@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Categoría</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Categoría</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="container-fluid">
						<div class="">
							<div class="my-4">
								<h4 class="text-center p-2" style="background-color: #f3f3f3; border-radius: 5px;">Agregar categoría</h4>
							</div>

							<div class="">
								@if(kvfj(Auth::user()->permissions, 'category_add'))
								<form action="{{ url('/admin/category/add/'.$module) }}" method="POST" enctype="multipart/form-data">
									@csrf
									<label for="name">Nombre categoría:</label>
									<div class="input-group">
										<input type="text" name="name" class="input-connect p-1" style="height: 30px !important; width: 500px;">
									</div>

									<label for="module" class="mt-3">Categoría Padre:</label>
									<div class="input-group">
										<select name="parent" class="input-connect p-1" style="height: 30px !important; width: 500px;">
											<option value="0">Sin categoría padre</option>
											@foreach($cats as $cat)
											<option value="{{ $cat->id }}">{{ $cat->name }}</option>
											@endforeach
										</select>
									</div>

									<label for="module" class="mt-3">Módulo:</label>
									<div class="input-group">
										<select name="module" class="input-connect p-1" style="height: 30px !important; width: 500px;" disabled>
											@foreach(getModulesArray() as $key => $value)
											<option value="{{ $key }}" {{ $module == $key ? 'selected' : '' }}>{{ $value }}</option>
											@endforeach
										</select>
									</div>

									<button type="submit" class="btn btn-outline-dark mt-4 container">Guardar</button>
								</form>
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-9">
					<div class="container-fluid mt-5">
						<div class="">

							<div class="inside">
								<nav class="nav nav-pills nav-fill" style="background-color: #f3f3f3;">
									@foreach(getModulesArray() as $m => $k)
										<a class="nav-link" href="{{ url('/admin/categories/'.$m) }}">
											{{ $k }}
										</a>
									@endforeach
								</nav>
								<table class="table mtop16">
									<thead>
										<tr>
											<th>Id</th>
											<th>Nombre</th>
											<th>Acción</th>
										</tr>
									</thead>
									<tbody>
										@foreach($cats as $cat)
										<tr>
											<td>{{ $cat->id }}</td>
											<td>{{ $cat->name }}</td>
											<td>
												<div class="opts">
													@if(kvfj(Auth::user()->permissions, 'category_subs'))
														<a href="{{ url('/admin/category/'.$cat->id.'/subs') }}" class="btn btn-outline-success mr-2" data-toggle="tooltip" data-placement="top" title="Subcategorías">
															Subcategoría
														</a>
														@endif
													@if(kvfj(Auth::user()->permissions, 'category_edit'))
														<a href="{{ url('/admin/category/'.$cat->id.'/edit') }}" class="btn btn-outline-dark mr-2" data-toggle="tooltip" data-placement="top" title="Editar">
															Editar
														</a>
													@endif
													@if(kvfj(Auth::user()->permissions, 'category_delete'))
													<a href="{{ url('/admin/category/'.$cat->id.'/delete') }}" class="btn btn-outline-danger mr-2" data-toggle="tooltip" data-placement="top" title="Eliminar">
														Eliminar
													</a>
													@endif
												</div>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
    </div>
</main>
