@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
		@if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Sub categoría <strong>{{ $category->name }}</strong></h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ url('/admin/categories/0') }}">Categoría</a></li>
                <li class="breadcrumb-item active">Sub Categoría {{ $category->name }}</li>
                </ol>
            </nav>
        </div>
        
        <div class="container">

            <div class="">
                    <table class="table">
                        <thead>
                            <tr>
                                {{-- <td></td> --}}
                                <td>Nombre</td>
                                <td>Acción</td>
                            </tr>
                        </thead>

                        <tbody class="">
                            @foreach($category->getSubCategories as $cat)
                            <tr>
                                {{-- <td>
                                    @if(!is_null($cat->icono))
                                    <img src="{{ url('/upload/'.$cat->file_path.'/'.$cat->icono) }}" class="img-fluid">
                                    @endif
                                </td> --}}
                                <td>{{ $cat->name }}</td>
                                <td>
                                    <div>
                                        
                                        @if(kvfj(Auth::user()->permissions, 'category_edit'))
                                            <a href="{{ url('/admin/category/'.$cat->id.'/edit') }}" class="btn btn-outline-dark mr-2" data-toggle="tooltip" data-placement="top" title="Editar">
                                                Editar
                                            </a>
                                        @endif
                                        @if(kvfj(Auth::user()->permissions, 'category_delete'))
                                            <a href="{{ url('/admin/category/'.$cat->id.'/delete') }}" class="btn btn-outline-danger mr-2" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                                Eliminar
                                            </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

            </div>
        </div>
	</div>

</main>