@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
        {{-- messages error --}}
        @if(Session::has('message'))
            <div class="alert alert-{{ Session::get('typealert') }}">
                {{ Session::get('message') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!--Page Title -->
        <div class="pagetitle">
            <h1>Multimedia</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Configuración multimedia</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->

        <div class="container-fluid">
            <div class="">
                <h4 class="text-center" style="background-color: #f3f3f3; border-radius: 5px;">Agregar video de inicio</h4>
            </div>

            <div class="row mb-4">
                <div class="col-md-3">
                    <div class="container-fluid">
                        <div class="">
                            <div class="">
                                @if(kvfj(Auth::user()->permissions, 'video_add'))
                                <form action="{{ url('/admin/multimedia/upload') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div>
                                        <label for="videoUpload">Subir video:</label>
                                        <div>
                                            <input type="file" class="form-control" id="videoUpload" name="video_url" accept="video/*" required>
                                        </div>
                                    </div>

                                    <div class="my-2">
                                        <label for="title_video">Título video:</label>
                                        <div class="input-group">
                                            <input type="text" name="title_video" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>
                                    </div>

                                    <div class="my-2">
                                        <label for="description_video">Descripción del video:</label>
                                        <div class="input-group">
                                            <input type="text" name="description_video" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>
                                    </div>

                                    <div class="my-2">
                                        <label for="link_video">Link de video:</label>
                                        <div class="input-group">
                                            <input type="text" name="link_video" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-outline-dark mt-4 container">Guardar</button>
                                </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class=" mt-3">
                        <div class="">
                            <div class="inside">
                                <div class="videoSection">
                                    @if ($multimedia)
                                        <div class="containerVideo">
                                            <video src="{{ asset('storage/video/uploads_video_home/home.mp4') }}" autoplay muted loop controls></video>
                                        </div>
                                        <div class="overlay-section d-flex flex-column align-items-start justify-content-center p-3">
                                            <h1 class="title-home mx-4">{{ $multimedia->title }}</h1>
                                            <p class="text-home mx-5">{{ $multimedia->description }}</p>
                                            <span class="text-home mx-5">Url guardada: {{ $multimedia->link_url }}</span>
                                        </div>
                                    @else
                                        <p>No hay video disponible</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
