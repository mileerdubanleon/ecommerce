@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Inventario - {{ $product->name }}</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/admin/products/all') }}"> Productos</a></li>
                <li class="breadcrumb-item active">Inventario: {{ $product->name }}</li>
                </ol>
            </nav>
        </div>

        <div class="container-fluid">
			<div class="row">
                {{-- Columna 1 --}}
                <div class="col-md-3">
                    <div class="container">
                        <span class="py-2 px-2 d-flex" style="background-color: #f3f3f3">Crear inventario</span>
                    </div>

                    <div class="container">
                        <form action="{{ url('/admin/product/'.$product->id.'/inventory') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="my-2">
                                <span>Nombre:</span>
                                <input type="text" class="w-100" name="name" required>
                            </div>

                            <div class="my-2">
                                <span>Cantidad en inventario:</span>
                                <input type="number" class="w-100" name="inventory" >
                            </div>

                            <div class="my-2">
                                <span>Precio ({{ config('cms.currency') }}): </span>
                                <input type="number" placeholder="{{ config('cms.currency') }}" class="w-100" name="price" required>
                            </div>

                            <div class="my-2">
                                <span>Inventario sin límite:</span>
                                <select id="limited" class="w-100" name="limited">
                                    <option value="0" selected>Limitado</option>
                                    <option value="1">Ilimitado</option>
                                </select>
                            </div>

                            <div class="my-2">
                                <span>Inventario minimo: </span>
                                <input type="number" placeholder="Stock" class="w-100" name="minimum" >
                            </div>

                            <button type="submit" class="btn btn-success">Guardar</button>
                        </form>
                    </div>

                </div>

                {{-- Columna 2 --}}
                <div class="col-md-9">
					<div class="container-fluid mt-5">
						<div class="">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Existencias</th>
                                        <th>Mínimo</th>
                                        <th>Precio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($product->getInventory as $inventory)
                                        <tr>
                                            <td>{{ $inventory->id }}</td>
                                            <td>{{ $inventory->name }}</td>
                                            <td>
                                                @if($inventory->limited == "1")
                                                    Ilimitado
                                                @else
                                                    {{ $inventory->quantity }}
                                                @endif
                                            </td>
                                            <td>
                                                @if($inventory->limited == "1")
                                                    Ilimitado
                                                @else
                                                    {{ $inventory->minimum }}
                                                @endif
                                            </td>
                                            <td>{{ config('cms.currency') }} {{ $inventory->price }}</td>
                                            <td>
                                                <div class="opts">
                                                    @if(kvfj(Auth::user()->permissions, 'product_inventory_edit'))
                                                        <a href="{{ url('/admin/product/'.$inventory->id.'/edit_inventory') }}" title="Editar" class="btn btn-outline-dark">
                                                            Editar
                                                        </a>
                                                    @endif
                                                    @if(kvfj(Auth::user()->permissions, 'product_inventory_delete'))
                                                        <a href="{{ url('/admin/product/'.$inventory->id.'/delete_inventory') }}" class="btn btn-outline-danger mr-2" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                                            Eliminar
                                                        </a>
													@endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
						</div>
					</div>
				</div>

            </div>    
        </div>

	</div>
</main>