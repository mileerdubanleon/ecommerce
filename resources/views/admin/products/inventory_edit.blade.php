@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Editar - {{ $inventory->name }}</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/admin/products/all') }}"> Productos</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/admin/product/'.$inventory->getProduct->id.'/inventory') }}"> {{ $inventory->getProduct->name }}</a></li>
                <li class="breadcrumb-item active">Editar Inventario: {{ $inventory->getProduct->name }}</li>
                </ol>
            </nav>
        </div>

        <div class="container-fluid">
			<div class="row">
                {{-- Columna 1 --}}
                <div class="col-md-3">
                    <div class="container">
                        <span class="py-2 px-2 d-flex" style="background-color: #f3f3f3">Editar inventario</span>
                    </div>

                    <div class="container">
                        <form action="{{ url('/admin/product/'.$inventory->id.'/edit_inventory') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="my-2">
                                <span>Nombre:</span>
                                <input type="text" class="w-100" name="name" value="{{ $inventory->name }}" required>
                            </div>

                            <div class="my-2">
                                <span>Cantidad en inventario:</span>
                                <input type="number" class="w-100" name="inventory" value="{{ $inventory->quantity }}" required>
                            </div>

                            <div class="my-2">
                                <span>Precio ({{ config('cms.currency') }}): </span>
                                <input type="number" placeholder="{{ config('cms.currency') }}" value="{{ $inventory->price }}" class="w-100" name="price" required>
                            </div>

                            <div class="my-2">
                                <span>Inventario sin límite:</span>
                                <select id="limited" class="w-100" name="limited" value="{{ $inventory->limited }}">
                                    <option value="0">Limitado</option>
                                    <option value="1">Ilimitado</option>
                                </select>
                            </div>

                            <div class="my-2">
                                <span>Inventario minimo: </span>
                                <input type="number" placeholder="Stock" class="w-100" name="minimum" value="{{ $inventory->minimum }}" required>
                            </div>

                            <button type="submit" class="btn btn-success">Actualizar</button>
                        </form>
                    </div>

                </div>
                <div class="col-md-9">
                    <div class="container-fluid mt-2">
                        <span class="mx-2 h3 ">Variantes</span>
                        {{-- Form variants --}}
                        <form class="row" action="{{ url('/admin/product/'.$inventory->id.'/variant_product') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" name="name" class="my-2 mx-3" placeholder="Nombre variante">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-success">Guardar</button>
                            </div>
                        </form>
						<div class="py-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre variante</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($inventory->getVariants as $variant)
                                        <tr>
                                            <td>{{ $variant->id }}</td>
                                            <td>{{ $variant->name }}</td>
                                            <td>
                                                {{-- @if(kvfj(Auth::user()->permissions, 'product_inventory_edit'))
                                                    <a href="{{ url('/admin/product/'.$inventory->id.'/edit_inventory') }}" title="Editar" class="btn btn-outline-dark">
                                                        Editar
                                                    </a>
                                                @endif --}}
                                                @if(kvfj(Auth::user()->permissions, 'variant_delete'))
                                                    <a href="{{ url('/admin/product/'.$variant->id.'/variant/delete') }}" class="btn btn-outline-danger mr-2" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                                        Eliminar
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
						</div>
					</div>
                </div>
            </div>    
        </div>

	</div>
</main>