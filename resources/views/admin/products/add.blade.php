@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
		<!--Page Title -->
        <div class="">
            <h1>Productos</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Productos</li>
                </ol>
            </nav>
        </div>

		<div class="container-fluid">
			<div class=" ">
		
				<div class="inside">
					<form action="{{ url('/admin/product/add') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="row">
							<div class="col-md-3">
								<div class="mx-2 my-3">
									<input type="text" placeholder="Nombre del producto" name="name" class="form-control" >
								</div>
							</div>
		
							<div class="col-md-3 d-flex">
								<label for="category" class="mx-2 mt-4">Categoría:</label>
								<div class="input-group mx-2 my-3">
									<select name="category" id="category" class="form-control">
										@foreach($cats as $key => $value)
											<option value="{{ $key }}">{{ $value }}</option>
										@endforeach
									</select>
									<input type="text" 
											hidden 
											id="subcategory_actual" 
											name="subcategory_actual" >
								</div>
							</div>
							<div class="col-md-3 d-flex">
								<label for="subcategory" class="mx-2 mt-4">Subcategoría:</label>
								<div class="input-group mx-2 my-3">
									<select name="subcategory" id="subcategory" class="form-control" required>
										<!-- Las subcategorías se llenarán dinámicamente aquí -->
									</select>
								</div>
							</div>

						</div>
		
						<div class="row">

							<div class="col-md-5 my-3">
								<label for="name">Imagen Destacada:</label>
								<div class="form-file">
									<input class="form-control" name="image" type="file" id="image" accept="image/*">
									<label for="formFileDisabled" class="form-label"></label>
								</div>
							</div>
		
							<div class="col-md-2">
								<label for="indiscount">¿En descuento:</label>
								<div class="input-group" >
									<select name="indiscount" class="form-control"  >
										<option value="0">No</option>
										<option value="1">Sí</option>
									</select>
								</div>
							</div>
		
							<div class="col-md-2">
								<label for="discount">Descuento:</label>
								<div class="input-group">
									<input type="number" name="discount" class="form-control " min="0.00" step="any">
								</div>
							</div>

							<div class="col-md-3">
								<label for="code">Código de sistema:</label>
								<div class="input-group">
									<input type="text" name="code" class="form-control " >
								</div>
							</div>
						</div>

						<div class="row mt-4">
							<div class="col-md-12">
								<label for="content">Descripción:</label>
								<textarea name="content" class="form-control my-2" id="editor"></textarea>
							</div>
						</div>
		
						<div class="row mtop16">
							<div class="col-md-12">
								<button type="submit" class="btn btn-success">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		

	</div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script>
    $(document).ready(function(){
        editor_init('content');
    })

    function editor_init(field){
        CKEDITOR.replace(field,{
            toolbar: [
                { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ]},
                { name: 'basicstyles', items: ['Bold', 'Italic', 'BulletedList', 'Strike', 'Image', 'link', 'Unlink', 'Blockquote' ]},
                { name: 'document', items: ['CodeSnippet', 'EmojiPanel', 'preview', 'Source'] }
            ]
        });
    }
</script>
