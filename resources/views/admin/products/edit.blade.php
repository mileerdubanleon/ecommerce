@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<!-- Incluye la hoja de estilos de Fancybox -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" />

<main id="main" class="main">
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		<!--Page Title -->
        <div class="">
            <h1>Producto: {{ $p->name }}</h1>
            <nav>
                <ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ url('/admin') }}">Dashboard</a>
					</li>
					<li class="breadcrumb-item">
						<a href="{{ url('/admin/products/all') }}">Productos</a>
					</li>
					<li class="breadcrumb-item active">Editar</li>
                </ol>
            </nav>
        </div>

		<div class="container-fluid">
			<div class="row">
				<!-- Columna para editar información del producto -->
				<div class="col-md-6">
					<form action="/admin/product/{{ $p->id }}/edit" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="row mx-4">
							<div class="col-md-6">
								<label for="name">Nombre del producto:</label>
								<input type="text" name="name" value="{{ $p->name }}" class="form-control">
							</div>
							
						</div>

						<div class="row my-3 mx-4">
							<div class="col-md-6">
								<label for="category">Categoría:</label>
								<select name="category" id="category" class="form-control">
									@foreach($cats as $key => $value)
										<option value="{{ $key }}" {{ $p->category_id == $key ? 'selected' : '' }}>{{ $value }}</option>
									@endforeach
								</select>
								<input type="text" hidden id="subcategory_actual" name="subcategory_actual" value="{{ $p->subcategory_id }}">
							</div>
							<div class="col-md-6">
								<label for="subcategory">Subcategoría:</label>
								<select name="subcategory" id="subcategory" class="form-control" required>
									<option value="{{ $key }}" {{ $p->subcategory_id == $key ? 'selected' : '' }}></option>
								</select>
							</div>
						</div>
					
						<div class="row my-3 mx-4">
							<div class="col-md-4">
								<label for="indiscount" >¿En descuento?:</label>
								<select name="indiscount" class="form-control">
									<option value="0" {{ $p->in_discount == 0 ? 'selected' : '' }}>No</option>
									<option value="1" {{ $p->in_discount == 1 ? 'selected' : '' }}>Sí</option>
								</select>
							</div>
		
							<div class="col-md-4">
								<label for="discount">Descuento:</label>
								<input type="number" name="discount" value="{{ $p->discount }}" class="form-control" min="0.00" step="any">
							</div>

							<div class="col-md-4">
								<label for="discount">{{ config('cms.currency') }}Precio:</label>
								<input type="number" name="price" value="{{ $p->price }}" class="form-control" step="any">
							</div>
						</div>
		
						<div class="row my-3 mx-4">
							<div class="col-md-6">
								<label for="status">Estado:</label>
								<select name="status" class="form-control">
									<option value="0" {{ $p->status == 0 ? 'selected' : '' }}>Borrador</option>
									<option value="1" {{ $p->status == 1 ? 'selected' : '' }}>Publico</option>
								</select>
							</div>
						</div>
		
						<div class="row my-3 mx-4">
							<div class="col-md-12">
								<label for="code">Código de sistema:</label>
								<input type="text" name="code" value="{{ $p->code }}" class="form-control">
							</div>
						</div>

						<div class="row my-3 mx-4">
							<div class="col-md-12">
								<label for="code">Fecha límite de descuento:</label>
								<input type="date" name="discount_until_date" value="{{ $p->discount_until_date }}" class="form-control">
							</div>
						</div>
		
						<div class="row my-3 mx-4">
							<div class="col-md-12">
								<textarea name="content" class="form-control" id="editor">{{ $p->content }}</textarea>
							</div>
						</div>
		
						<div class="row mx-4">
							<div class="col-md-12">
								<button type="submit" class="btn btn-success">Editar</button>
							</div>
						</div>
					</form>
				</div>
				<!-- Columna para organizar imágenes -->
				<div class="col-md-6"><!-- Div para mostrar mensajes de error -->
					<div id="error-messages" class="text-danger"></div>

					<br>

					<!-- Panel de Imagen Destacada y Galería -->
					@include('admin.components.dropzone')
				
					<!-- Galería de imágenes -->
					@if(isset($images) && count($images) > 0)

						<div class="row">
							@foreach($images as $image)
								<div class="col-md-4 my-1">
									<a href="{{ asset('storage/img/uploads_product/'. $image->file_path) }}" data-fancybox="gallery">
										<img src="{{ asset('storage/img/uploads_product/'. $image->file_path) }}" class="img-fluid w-100 h-50 " alt="{{ $image->file_name }}">
									</a>
									
									<form method="POST" action="{{ route('product.gallery.destroy', ['productId' => $productId, 'imageId' => $image->id]) }}">
										@csrf
										@method('DELETE')
										<button type="submit" class="btn btn-danger mt-2">Eliminar</button>
									</form>
								</div>
							@endforeach
						</div>

					@endif

				</div>
			</div>
		</div>
	</div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<!-- Incluye la biblioteca de jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<!-- Incluye la biblioteca de Fancybox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

<script>
	// Textarea style
    $(document).ready(function(){
		$("[data-fancybox='gallery']").fancybox({
            // Configuración opcional aquí
        });

        editor_init('content');
    })

    function editor_init(field){
        CKEDITOR.replace(field,{
            toolbar: [
                { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ]},
                { name: 'basicstyles', items: ['Bold', 'Italic', 'BulletedList', 'Strike', 'Image', 'link', 'Unlink', 'Blockquote' ]},
                { name: 'document', items: ['CodeSnippet', 'EmojiPanel', 'preview', 'Source'] }
            ]
        });
    }

</script>
