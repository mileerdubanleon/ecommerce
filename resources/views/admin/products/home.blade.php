@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<!-- Incluye la hoja de estilos de Fancybox -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" />

<main id="main" class="main">
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Productos</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Productos</li>
                </ol>
            </nav>
        </div>

		<div class="container-fluid">
			<div class="">
				<div class="header">
					<div class="" style="list-style: none;">
						@if(kvfj(Auth::user()->permissions, 'product_add'))
	
							<a href="{{ url('/admin/product/add') }}" class="btn btn-outline-dark my-4">
								Agregar producto
							</a>
					
						@endif
							<div class="dropdown float-right">
								<button class="btn btn-outline-dark dropdown-toggle" type="button" id="btn_filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Filtrar <i class="fas fa-chevron-down"></i>
								</button>
								<ul class="dropdown-menu" aria-labelledby="btn_filter">
									<li><a class="dropdown-item" href="{{url('/admin/products/1')}}">Públicos</a></li>
									<li><a class="dropdown-item" href="{{url('/admin/products/0')}}">Borradores</a></li>
									<li><a class="dropdown-item" href="{{url('/admin/products/trash')}}">Papelera</a></li>
									<li><a class="dropdown-item" href="{{url('/admin/products/all')}}">Todos</a></li>
								</ul>
							</div>
						
						{{-- <li>
							<a href="#" id="btn_search">
								<i class="fas fa-search"></i> Buscar
							</a>
						</li> --}}
					</div>
				</div>

				<div class="inside">

					<div class="form_search mb-4" id="form_search">
						<form action="{{ url('/admin/product/search') }}" method="POST">
							@csrf
							<div class="row">
								<div class="col-md-4">
									<input type="text" name="search" class="form-control" placeholder="Ingrese su búsqueda">
								</div>
								<div class="col-md-4">
									<select name="filter" class="form-control">
										<option value="0">Nombre del producto</option>
										<option value="1">Código</option>
									</select>
								</div>
								<div class="col-md-2">
									<select name="status" class="form-control">
										<option value="0">Borrador</option>
										<option value="1">Públicos</option>
									</select>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-primary">Buscar</button>
								</div>
							</div>
						</form>
					</div>
					
					<table class="table table-striped">
						<thead>
							<tr>
								<td>ID</td>
								<td>Imagen</td>
								<td>Nombres</td>
								<td>Precio min.</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							@foreach($products as $p)
							<tr>
								<td>{{ $p->id }}</td>
								<td>
									@if($p->image)
										<a href="{{ asset('storage/img/uploads_product_image/'. $p->image) }}" data-fancybox="gallery">
											<img src="{{ asset('storage/img/uploads_product_image/'. $p->image) }}" width="64px">
										</a>
									@endif
								</td>
								<td class="">
									{{ $p->name }} 
										@if($p->status == "0") 
											<span style="font-size: 10px;" class="bg-info rounded p-1">Borrador</span> 
											@else
												@if($p->status == "1")
													<span style="font-size: 10px;" class="bg-success rounded p-1">Publico</span> 
												@endif
										@endif
									<p class="my-2">
										<small>
											{{ $p->cat->name }}
											
											@if($p->subcategory_id != "0")
												>>
												{{ $p->getSubcategory->name }}
											@endif
										</small>
									</p>
								</td>
								<td>
									{{ config('cms.currency') }}{{ $p->price }}
								</td>
								<td>
									@if(kvfj(Auth::user()->permissions, 'product_edit'))
										<a href="{{ url('/admin/product/'.$p->id.'/edit') }}" title="Editar" class="btn btn-outline-dark">
											<i class="bi bi-pencil"></i>
										</a>
									@endif
									@if(kvfj(Auth::user()->permissions, 'product_inventory'))
										<a href="{{ url('/admin/product/'.$p->id.'/inventory') }}" title="Inventario" class="btn btn-outline-warning mx-2">
											<i class="bi bi-box-seam"></i>
										</a>
									@endif
									@if(kvfj(Auth::user()->permissions, 'product_delete'))
										@if(is_null($p->deleted_at))
											<a href="{{ url('/admin/product/'.$p->id.'/delete') }}" 
												data-path="admin/product" 
												data-action="delete" 
												data-object="{{ $p->id }}" 
												data-toggle="tooltip" 
												data-placement="top" 
												title="Eliminar" 
												class="btn-deleted btn btn-outline-danger"
												onclick="confirmDelete('{{ $p->id }}', event)"
											>
												<i class="bi bi-trash"></i>
											</a>
										@else
											<a href=" {{ url('/admin/product/'.$p->id.'/restore') }}" 
												data-path="admin/product" 
												data-action="restore" 
												data-object="{{ $p->id }}" 
												data-toggle="tooltip" 
												data-placement="top" 
												title="Restaurar" 
												class="btn-deleted">
													<i class="bi bi-arrow-counterclockwise"></i>
											</a>
										@endif
									@endif
								</td>
							</tr>
							@endforeach
							<tr>
								<td colspan="1" class="text-center">
									{!! $products->render() !!}
								</td>
							</tr>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</main>
	<!-- ./wrapper -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->

	<!-- Incluye la biblioteca de jQuery -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

	<!-- Incluye la biblioteca de Fancybox -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

<script>        // sweet alert
	function confirmDelete(id, event) {
		event.preventDefault();  // Evitar el comportamiento predeterminado del enlace
		
		Swal.fire({
			title: '¿Estás seguro?',
			text: 'Esta acción no se puede deshacer',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#d33',
			cancelButtonColor: '#3085d6',
			confirmButtonText: 'Sí, eliminar'
		}).then((result) => {
			if (result.isConfirmed) {
				window.location.href = '{{ url("/admin/product") }}/' + id + '/delete';
			}
		});
	}

	// Inicializa Fancybox en todos los elementos con el atributo data-fancybox="gallery"
    $(document).ready(function() {
        $("[data-fancybox='gallery']").fancybox({
            // Configuración opcional aquí
        });
    });
</script>