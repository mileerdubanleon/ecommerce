@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Configuración carrusel</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Configuración Carrusel</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
		<div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
					<div class="container-fluid">
						<div class="">
							<div class="my-4">
								<h4 class="text-center p-2" style="background-color: #f3f3f3; border-radius: 5px;">Agregar carrusel</h4>
							</div>

							<div class="">
								@if(kvfj(Auth::user()->permissions, 'carrusel_add'))
                                    <form action="{{ url('/admin/carrusel/add/') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <label for="name">Título:</label>
                                        <div class="input-group">
                                            <input type="text" name="title" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>

                                        <label for="name">Descripción:</label>
                                        <div class="input-group">
                                            <input type="text" name="description" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>

                                        <label for="name">Texto enlace:</label>
                                        <div class="input-group">
                                            <input type="text" name="textla" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>

                                        <label for="name">Link enlace:</label>
                                        <div class="input-group">
                                            <input type="text" name="linkla" class="input-connect p-1" style="height: 30px !important; width: 500px;">
                                        </div>


                                        <button type="submit" class="btn btn-outline-dark mt-4 container">Guardar</button>
                                    </form>
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-9">
					<div class="container-fluid mt-5">
						<div class="">

							<div class="inside">
								<table class="table mtop16">
									<thead>
										<tr>
											<th>Id</th>
											<th>Título</th>
											<th>Descripción</th>
                                            <th>Texto enlace</th>
                                            <th>Link de texto</th>
										</tr>
									</thead>
									<tbody>
										@foreach($carr as $car)
                                            <tr>
                                                <td>{{ $car->id }}</td>
                                                <td>{{ $car->text1 }}</td>
                                                <td>{{ $car->text2 }}</td>
                                                <td>{{ $car->link_text }}</td>
                                                <td>{{ $car->link_url }}</td>
                                                <td>
                                                    <div class="opts">
                                                        @if(kvfj(Auth::user()->permissions, 'carrusel_edit'))
                                                            <a href="{{ url('/admin/carrusel/'.$car->id.'/edit') }}" class="btn btn-outline-dark mr-2"  title="Editar">
                                                                Editar
                                                            </a>
                                                        @endif
                                                        @if(kvfj(Auth::user()->permissions, 'carrusel_delete'))
                                                            <a href="{{ url('/admin/carrusel/'.$car->id.'/delete') }}" class="btn btn-outline-danger mr-2" title="Eliminar">
                                                                Eliminar
                                                            </a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
		
    </div>
</main>