@extends('admin.index')

{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')
{{-- content --}}

<main id="main" class="main container p-4" >

    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="">
        <div class="row">
            <!-- Recent Activity -->
            <div class="col-md-3 mb-3">
                <div class="p-4 card border-secondary shadow">
                    <div class="card-header">Usuarios</div>
                    <div class="card-body text-secondary text-center">
                        <h5 class="card-title">No. 23</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        
            <div class="col-md-3 mb-3">
                <div class="p-4 card border-secondary shadow">
                    <div class="card-header">Categorias</div>
                    <div class="card-body text-secondary text-center">
                        <h5 class="card-title">No. 15</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        
            <div class="col-md-3 mb-3">
                <div class="p-4 card border-secondary shadow">
                    <div class="card-header">Productos</div>
                    <div class="card-body text-secondary text-center">
                        <h5 class="card-title">No. 15</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>

        </div>
        
    </section>

</main><!-- End #main -->