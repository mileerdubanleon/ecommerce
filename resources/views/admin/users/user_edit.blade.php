@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
	<div class="container">
        <h1>Editar usuario: {{ $u->name }} {{ $u->lastname }}</h1>
        <nav>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/users/all') }}">Usuarios</a></li>
			<li class="breadcrumb-item active">Editar usuario: <em>{{ $u->name }}</em> </li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <div class="container">
		{{-- messages error --}}
		@if(Session::has('message'))
			<div class="alert alert-{{ Session::get('typealert') }}">
				{{ Session::get('message') }}
			</div>
		@endif
		
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body mt-3 col-md-12">
				<div class="p-3 row">

					<div class="col-md-6 mb-5 p-4">
					
						<div class="row">
							{{-- @if(is_null($u->avatar))
								<img src="{{ url('/static/images/default-avatar.png') }}" class="avatar">
							@else
								<img src="{{ url('/uploads_users/'.$u->id.'/'.$u->avatar) }}" class="avatar">
							@endif --}}
					
							<div class="col">
								<div class="row mb-2">
									<div class="col">
										<span class="font-weight-bold"> Nombre:</span>
										<span class="text">{{ $u->name }} {{ $u->lastname }} </span>
									</div>
								</div>
					
								<div class="row mb-2">
									<div class="col">
										<span class="font-weight-bold"> Estado del usuario:</span>
										<span class="text">{{ getUserStatusArray(null,$u->status) }}</span>
									</div>
								</div>
					
								<div class="row mb-2">
									<div class="col">
										<span class="font-weight-bold">Correo electrónico:</span>
										<span class="text">{{ $u->email }}</span>
									</div>
								</div>
					
								<div class="row mb-2">
									<div class="col">
										<span class="font-weight-bold">Fecha de registro:</span>
										<span class="text">{{ $u->created_at }}</span>
									</div>
								</div>
					
								<div class="row mb-2">
									<div class="col">
										<span class="font-weight-bold"> Role de usuario:</span>
										<span class="text">{{ getRoleUserArray(null,$u->role) }}</span>
									</div>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col">
								@if(kvfj(Auth::user()->permissions, 'user_banned'))
									@if($u->status  == "100")
										<a href="{{ url('/admin/user/'.$u->id.'/banned') }}" class="btn btn-success">Activar Usuario</a>
									@else
										<a href="{{ url('/admin/user/'.$u->id.'/banned') }}" class="btn btn-danger">Suspender Usuario</a>
									@endif
								@endif
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="">
	
							<form action="/admin/user/{{ $u->id }}/edit" method="POST">
								@csrf
								<div class="row">
									<div class="col-md-6">
										<label class="my-3">Tipo de usuario:</label>
										<div class="input-group">
											<select name="user_type" class="form-control">
												@foreach(getRoleUserArray('list', null) as $key => $value)
													<option value="{{ $key }}" {{ $key == $u->role ? 'selected' : '' }}>{{ $value }}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							
								<div class="row my-3">
									<div class="col-md-12">
										<button type="submit" class="btn btn-success">Guardar</button>
									</div>
								</div>
							</form>
							
	
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</main>
