@extends('admin.index')
{{-- Cabecera web --}}
@include('layout.nav.head')
{{-- Navbar --}}
@include('layout.nav.nav')
{{-- sidebar --}}
@include('admin.layout.sidebar')

<main id="main" class="main">
    <div class="container">
		<!--Page Title -->
        <div class="pagetitle">
            <h1>Productos</h1>
            <nav>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Productos</li>
                </ol>
            </nav>
        </div>

		<div class="container-fluid">
			<div class="">
				<div class="header">
					<h2 class="title"><i class="fas fa-cogs"></i> Configuraciones</h2>
				</div>

				<div class="inside">
					<form action="{{ url('/admin/settings/add') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="row">
							<div class="col-md-4">
								<label for="name">Nombre de la tienda:</label>
								<div class="input-group">
									<input type="text" name="name" class="form-control" value="{{ Config::get('cms.name') }}">
								</div>
							</div>
							<div class="col-md-4">
								<label for="currency">Moneda:</label>
								<div class="input-group">
									<input type="text" name="currency" class="form-control" value="{{ Config::get('cms.currency') }}">
								</div>
							</div>

							<div class="col-md-4">
								<label for="company_phone">Teléfono:</label>
								<div class="input-group">
									<input type="number" name="company_phone" class="form-control" value="{{ Config::get('cms.company_phone') }}">
								</div>
							</div>
						</div>

						<div class="row my-2">
							<div class="col-md-4">
								<label for="map">Ubicaciones:</label>
								<div class="input-group">
									<input type="text" name="map" class="form-control" value="{{ Config::get('cms.map') }}">
								</div>
							</div>

							<div class="col-md-3">
								<label for="maintenance_mode">Modo mantenimiento:</label>
								<div class="input-group">
									<select name="maintenance_mode" class="form-control">
										<option value="0" @if(Config::get('cms.maintenance_mode') == '0') selected @endif>Desactivado</option>
										<option value="1" @if(Config::get('cms.maintenance_mode') == '1') selected @endif>Activado</option>
									</select>
								</div>
							</div>
						</div>

						<div class="row my-2">
							<div class="col-md-4">
								<label for="products_per_page">Productos para mostrar por página:</label>
								<div class="input-group">
									<input type="number" name="products_per_page" class="form-control" value="{{ Config::get('cms.products_per_page') }}" min="1" required>
								</div>
							</div>
							<div class="col-md-4">
								<label for="products_per_page_random">Productos para mostrar por página (Random):</label>
								<div class="input-group">
									<input type="number" name="products_per_page_random" class="form-control" value="{{ Config::get('cms.products_per_page_random') }}" min="1" required>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-success">Guardar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>
</main>		
