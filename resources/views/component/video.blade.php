<div class="videoSection">
    <div class="containerVideo">
        <video src="{{ asset('storage/video/uploads_video_home/home.mp4') }}" autoplay muted loop></video>
    </div>
    @if ($multimedia)
    <div class="overlay-section d-flex flex-column align-items-start justify-content-center p-3">
        <h1 class="title-home mx-4">{{ $multimedia->title }}</h1>
        <p class="text-home mx-5">{{ $multimedia->description }}</p>
    </div>
    <a href="{{ $multimedia->link_url }}" class="video-link"></a>
    @else
    <div class="overlay-section d-flex flex-column align-items-start justify-content-center p-3">
        <h1 class="title-home mx-4">Tienda online</h1>
        <p class="text-home mx-5">Tu texto descriptivo aquí.</p>
    </div>
    <a href="https://www.tu-dominio.com/otro-apartado" class="video-link"></a>
    @endif
</div>